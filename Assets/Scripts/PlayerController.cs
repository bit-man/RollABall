﻿using System.Collections;
using UnityEngine;
using System;

public class PlayerController : MonoBehaviour {

	public float keyboardSpeed;
	public float touchSpeed;
	private Rigidbody rb;

	void Start() {
			rb = GetComponent<Rigidbody>();
	}

	void FixedUpdate () {
		keyboardInput();
		touchScreenInput();
	}

	void keyboardInput () {
			float moveHorizontal = Input.GetAxis("Horizontal");
			float moveVertical = Input.GetAxis("Vertical");

			Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

			rb.AddForce(movement * keyboardSpeed);
	}

	void touchScreenInput() {
		foreach (Touch touch in Input.touches) {
			if (touch.phase == TouchPhase.Began) {
				Vector2 position = touch.position;

				float moveHorizontal = Math.Sign(position.x - (Screen.width / 2));
				float moveVertical = Math.Sign(position.y - (Screen.height / 2));

				Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

				rb.AddForce(movement * touchSpeed);
			}
		}
	}
}
